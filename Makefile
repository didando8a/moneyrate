MAKEFLAGS += --warn-undefined-variables
SHELL := /bin/bash
.SHELLFLAGS := -eu -o pipefail -c
.DEFAULT_GOAL := help
.DELETE_ON_ERROR:
.SUFFIXES:
PROJECT_NAME ?= moneyrate
USER_ID = $(shell id -u $$(shell whoami))
REVISION ?= 0.0.1-SNAPSHOT

PHONY: prepare.init
#prepare.init:  @ Clean git hooks copy .env.dist file into .env if .env does not exist
prepare.init:
	rm -f .git/hooks/pre-commit && rm -f .git/hooks/post-commit
	test -f .env || cp .env.dist .env

.PHONY: common.build
#common.build:  @ Compiles and package the code into a jar file
common.build:
	docker-compose --project-name ${PROJECT_NAME} -f docker-compose.maven.yml run --rm --user ${USER_ID} maven mvn clean package -Drevision=$$(git rev-parse HEAD) -Dmaven.test.skip=true -Duser.home=/var/maven --settings ./settings.xml

.PHONY: dev.maven.container.up
#dev.maven.container.up:  @ Create and run a mvn container with the current user
dev.maven.container.up:
	docker-compose --project-name ${PROJECT_NAME} -f docker-compose.maven.yml run --user ${USER_ID} --rm maven bash

.PHONY: dev.moneyrate.container.up
#dev.moneyrate.container.up:  @ Create the container for the application to run
dev.moneyrate.container.up:
	docker-compose --project-name ${PROJECT_NAME} up --build

.PHONY: dev.all.container.up
#dev.all.container.up:  @ Rebuild jar application file and Create the container for the application to run
dev.all.container.up: common.build dev.moneyrate.container.up

.PHONY: artifact.push-to-gitlab
#artifact.push-to-gitlab:  @ Push artifact to the gitlab maven repository
artifact.push-to-gitlab:
	docker-compose --project-name=${PROJECT_NAME} -f docker-compose.maven.yml run --rm --user ${USER_ID} maven mvn deploy -Drevision=$(REVISION) -Duser.home=/var/maven -e -U --settings ./settings.xml

.PHONY: docker.gitlab.login
#docker.gitlab.login:  @ Login into Gitlab container registry
docker.gitlab.login:
	docker login registry.gitlab.com

.PHONY: docker.gitlab.logout
#docker.gitlab.login:  @ Logout into Gitlab container registry
docker.gitlab.logout:
	docker logout registry.gitlab.com

.PHONY: dev.docker.clean
#dev.docker.clean:  @ Clean all docker containers NEEDS TO BE IMPROVED
dev.docker.clean:
	-docker rm $$(docker ps -qa)
	-yes | docker system prune -a
	yes | docker volume prune

.PHONY: help
#help:  @ List available tasks on this project
help:
	@grep -E '[a-zA-Z\.\-]+:.*?@ .*$$' $(MAKEFILE_LIST)| sort | tr -d '#'  | awk 'BEGIN {FS = ":.*?@ "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'