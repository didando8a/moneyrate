package com.didando8a.moneyrate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MoneyrateApplication {

	public static void main(String[] args) {
		SpringApplication.run(MoneyrateApplication.class, args);
	}

}
