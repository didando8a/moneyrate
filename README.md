# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.3.1.RELEASE/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.3.1.RELEASE/maven-plugin/reference/html/#build-image)

### Docker mvn
Currently the mvn docker container is meant to be run for the curren user in the maching running it (1000).
This way the target file is not owned by root and can easyly manipulated by the current user.

If the process (for example **make common.build**) still creates some file in target for only root:

* make sure the files in ~/.m2 (and the directory itself) are have 755 permissions if not execute **chmod 755 ~/.m2**
* make sure the files in PATH_TO_PROJECT/.m2 (and the directory itself) are have 755 permissions if not execute **CD PATH_TO_PROJECT && chmod 755 ~/.m2**
